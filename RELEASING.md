## Making packages

### Adjust version number

```
emacs setup.py

git commit -m "bump version"
git push origin master
```

### PyPi

For making source package, run

```
python3 setup.py sdist
```

For upload to PyPI:

```
twine upload dist/iocbio.fperiod-0.1.2.tar.gz
```

### Make release at Gitlab

Go to https://gitlab.com/iocbio/fperiod/tags and make a new release
with Changelog
