# FPeriod

Library for estimation of fundamental period of sequences.

This library was developed as a part of [IOCBio
project](https://github.com/iocbio/iocbio) and is moved into [separate
repository](https://gitlab.com/iocbio/fperiod) for easier use. Library
can be used from C or Python.

We ask to acknowledge the use of the software in scientific articles
by citing the following paper:

 Peterson, P., Kalda, M., & Vendelin, M. (2013).
 Real-time determination of sarcomere length of a single cardiomyocyte during contraction.
 American Journal of Physiology - Cell Physiology, 304(6), C519-C531.
 https://doi.org/10.1152/ajpcell.00032.2012

## Install

Install using pip3:

```
pip3 install --user git+https://gitlab.com/iocbio/fperiod
```

For upgrade, add `--upgrade` after install keyword. For example,
```
pip3 install --upgrade --user git+https://gitlab.com/iocbio/fperiod
```

## Authors

* Pearu Peterson, 2011, original code

* Marko Vendelin, 2019, adaptation using Cython, restructuring

From [Laboratory of Systems Biology, Department of Cybernetics, School of Science, 
Tallinn University of Technology](https://sysbio.ioc.ee).

## License

BSD, see LICENSE file.

